# Users of VISE

 * [RKD Netherlands Institute of Art History](https://rkd.nl/en/)
 * [Ashmolean Museum, Oxford](https://www.ashmolean.org/)
 * [Spanish Chapbooks Project at Cambridge University Library](https://www.cdh.cam.ac.uk/news/spanish-chapbooks)
 * Frank Scholten Project at the [University of Leiden](https://www.universiteitleiden.nl/en)
 * [Sistemas de Informação](https://sistemasfuturo.pt/) for the [Az Infinitum](https://inwebonline.net/azInfinitum/azinfinitum.aspx?pesquisaGeral=1) project at the Art Institute of the University of Lisboa
 * [Ca' Foscari University of Venice Lyon 16ci database of book illustrations printed in Venice](https://www.robots.ox.ac.uk/~vgg/research/16ci/lyon/)
 * [University College Dublin Ornamento database of book ornaments](https://ornamento.ucd.ie/)
 * [Compositor project at the University of Birmingham](https://compositor.bham.ac.uk)
 
***

Contact [Abhishek Dutta](mailto:adutta@robots.ox.ac.uk) for queries and feedback related to this page.
